# README #

#Requirement:
JDK 6 + NetBeans

#Version 1.0

* Fully working [GOL](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) Simulator 

* Randomly and manually fill cell

* Play/Pause the simulation

* Color base on futur cell state

#Installation and run

Clone the repo and you're good to go.


This is a NetBeans Project. Using NetBeans isnt mandatory but recommended for the buil in applet runner.
You can run the SuppaMarioApplet.java class with netbeans.


#Description

You will start in a premade map. 

![Scheme](images/start.png)

Just press play and enjoy.

![Scheme](images/started.png)

You can fill with random cells

![Scheme](images/filled.png)

Or just draw them. 
Left mouse click to add cells. Right to remove.

Cell color is based on theyr future stare. Red = dead, green = alive.


#License 


All my work is released under [DBAD](https://www.dbad-license.org/) license.