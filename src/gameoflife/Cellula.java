package gameoflife;

public class Cellula {
	
	boolean viva; //vero se viva
	//Color colore = Color.BLACK; //colore della cellula
	boolean destino; //lo stato successivo	
        
	//costruttore vuoto
	public Cellula(){
		this.viva = false;
		this.destino = false;
	}
	//costruttore con parametri
	public Cellula(boolean viva, boolean destino){
		this.viva = viva; 
		this.destino = destino;
	}
	
	//verifica lo stato della cellula
	public boolean diagnosi(){
		return this.viva;
	}
	
	//decide che vive
	public void vivi(){
		this.viva = true;
	}
	
	//decide che muore
	public void muori(){
		this.viva = false;
	}
	//decide che domani vivra
	public void vivrai(){
		this.destino = true;
	}
	
	//decide che domani sara morta
	public void morirai(){
		this.destino = false;
	}
	
	//inverte lo stato vitale
	public void toggleVita(){
		this.viva = !this.viva;
	}
	
	public void impostaDestino(){
		this.viva = this.destino;
	}
}
